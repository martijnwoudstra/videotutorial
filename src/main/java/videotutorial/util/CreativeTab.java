package videotutorial.util;

import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import videotutorial.lib.References;

public class CreativeTab
{
    public static CreativeTabs myCreativeTab = new CreativeTabs(References.MODID)
    {
        @Override
        public Item getTabIconItem()
        {
            return Items.apple;
        }
    };
}
