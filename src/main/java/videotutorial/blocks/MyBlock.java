package videotutorial.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.util.IIcon;
import videotutorial.lib.References;
import videotutorial.util.CreativeTab;

public class MyBlock extends net.minecraft.block.Block
{
    public IIcon[] icons;
    private String name = "myBlock";
    public MyBlock()
    {
        super(Material.wood);
        setBlockName(name);
        setHardness(1f);
        setResistance(4f);
        setCreativeTab(CreativeTab.myCreativeTab);
        icons = new IIcon[6];
        GameRegistry.registerBlock(this, name);
        setStepSound(Block.soundTypeWood);
        //MyChanges
    }

    @Override
    public void registerBlockIcons(IIconRegister iconRegister)
    {
        icons[0] = iconRegister.registerIcon(References.MODID + ":" + name + "Top"); //assets/textures/blocks/myBlockTop.png
        icons[1] = iconRegister.registerIcon(References.MODID + ":" + name + "Bot"); //assets/textures/blocks/myBlockBot.png
        for(int i = 2; i <= 5; i++)
        {
            icons[i] = iconRegister.registerIcon(References.MODID + ":" + name + "Side" + i); //assets/textures/blocks/myBlockSide2345.png
        }
    }

    @Override
    public IIcon getIcon(int side, int meta)
    {
        return icons[side];
    }
}
