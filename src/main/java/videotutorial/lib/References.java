package videotutorial.lib;

/**
 * References
 * Created by: Martijn Woudstra
 * Date: 23-apr-2014
 */

public class References
{
    public static final String MODID = "videotutorial";
    public static final String MODNAME = "Video Tutorial";
    public static final String VERSION = "0.0.1";
    public static final String CLIENTPROXY = "videotutorial.core.proxy.ClientProxy";
    public static final String COMMONPROXY = "videotutorial.core.proxy.CommonProxy";
}
