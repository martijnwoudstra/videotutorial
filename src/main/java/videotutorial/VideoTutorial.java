package videotutorial;

import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import videotutorial.blocks.ModBlocks;
import videotutorial.core.proxy.CommonProxy;
import videotutorial.items.ModItems;
import videotutorial.items.crafting.RecipeHandler;
import videotutorial.lib.References;

/**
 * VideoTutorial
 * Created by: Martijn Woudstra
 * Date: 23-apr-2014
 */

@Mod(modid = References.MODID, name = References.MODNAME, version = References.VERSION)
public class VideoTutorial
{

    @SidedProxy(serverSide = References.COMMONPROXY, clientSide = References.CLIENTPROXY)
    public static CommonProxy proxy;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event)
    {
        ModBlocks.init();

        ModItems.init();
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event)
    {
        RecipeHandler.init();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event)
    {

    }
}
