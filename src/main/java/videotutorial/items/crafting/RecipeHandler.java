package videotutorial.items.crafting;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.FurnaceRecipes;
import videotutorial.blocks.ModBlocks;
import videotutorial.items.ModItems;

public class RecipeHandler
{
    public static void init()
    {
        GameRegistry.addRecipe(new ItemStack(ModBlocks.myBlock, 1), "XXX","XDX","XXX", 'X', Blocks.dirt, 'D', Blocks.diamond_block);
        GameRegistry.addShapelessRecipe(new ItemStack(ModItems.myItem, 1), Blocks.dirt, Blocks.cactus);
        FurnaceRecipes.smelting().func_151393_a(ModBlocks.myBlock, new ItemStack(ModItems.myItem, 1), 1f);

        ItemStack itemStack = new ItemStack(Items.diamond_sword);
        itemStack.addEnchantment(Enchantment.efficiency, 2);
        GameRegistry.addRecipe(itemStack, "DDD","DXD", "DDD", 'D', Blocks.diamond_block, 'X', Blocks.dirt);
    }
}
