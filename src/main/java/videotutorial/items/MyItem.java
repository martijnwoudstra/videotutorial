package videotutorial.items;

import cpw.mods.fml.common.registry.GameRegistry;
import videotutorial.util.CreativeTab;

public class MyItem extends net.minecraft.item.Item
{
    private String name = "myItem";

    public MyItem()
    {
        setUnlocalizedName(name);
        setCreativeTab(CreativeTab.myCreativeTab);
        GameRegistry.registerItem(this, name);
    }
}
